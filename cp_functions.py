import numpy as np
import cvxpy as cp
from scipy.stats import truncnorm

def is_equality(constraint):
    return '==' in str(constraint)

def euclidean_distance(L1,L2):
    if len(L1) != len(L2):
        raise ValueError("Vectors must have the same length.")
    return np.sqrt(np.sum((L1 - L2)**2))

def simulate_TN(mu,sigma,lower_bound,upper_bound):
    a = (lower_bound - mu)/sigma
    b = (upper_bound - mu)/sigma
    res = truncnorm.rvs(a, b, loc = mu, scale = sigma, size=1)
    return res[0]

def U_feasible(W_current, current_state, Q, C, D):
    nb_link = 5
    nb_path = 3
    U = cp.Variable((nb_path))
    L = cp.Variable((nb_link))
    X = current_state
    constr = []
    # (6.1)
    for p in range(nb_path):
        constr += [0. <= U[p], U[p] <= W_current[p]]
    constr += [U[0] + X[0] + U[1] + X[1] - L[0] == 0., L[0] <= C[0]]
    constr += [U[2] + X[2] - L[1] == 0., L[1] <= C[1]]
    constr += [U[1] + X[1] - L[2] == 0., L[2] <= C[2]]
    constr += [U[0] + X[0] - L[3] == 0., L[3] <= C[3]]
    constr += [U[1] + X[1] + U[2] + X[2] - L[4] == 0., L[4] <= C[4]]    
    # (6.5)
    constr += [cp.inv_pos(C[0] - L[0]) + cp.inv_pos(C[3] - L[3]) <= D[0] ]
    constr += [cp.inv_pos(C[0] - L[0]) + cp.inv_pos(C[2] - L[2]) + cp.inv_pos(C[4] - L[4]) <= D[1]]
    constr += [cp.inv_pos(C[1] - L[1]) + cp.inv_pos(C[4] - L[4]) <= D[2]]
    # objective
    utility = 0. 
    # solve
    problem = cp.Problem(cp.Minimize(utility), constr)
    res = problem.solve(solver = cp.SCS)
    u = U.value
    return u

def myopic_policy(current_horizon, current_state, current_utility, W_bar, Q, C, D, alpha, sigma):
    nb_path = 3
    W_current = np.zeros(nb_path)
    next_state = np.zeros(nb_path)
    for t in range(current_horizon):
        for p in range(nb_path):
            W_current[p] = W_bar[p] + simulate_TN(0., sigma, -W_bar[p], W_bar[p])
        U_current = U_feasible(W_current, current_state, Q, C, D) 
        state_det = current_state + U_current
        #print(state_det)
        current_utility += state_det[0]**alpha[0]/alpha[0] + state_det[1]**alpha[1]/alpha[1] \
                                    + state_det[2]**alpha[2]/alpha[2]
        for p in range(nb_path):
            next_state[p] = state_det[p]*Q[p] + \
            simulate_TN(0,sigma,-state_det[p]*min(Q[p], 1-Q[p]),state_det[p]*min(Q[p], 1-Q[p]))
        current_state = next_state    
    return current_utility

def solve_cp_post(T, init, W_current, W_bar, Q, C, D, alpha):
    nb_link = 5
    nb_path = 3
    horizon = T
    X = cp.Variable((nb_path, horizon))
    U = cp.Variable((nb_path, horizon))
    L = cp.Variable((nb_link, horizon))
    constr = []
    utility = 0.
    # Initial time-step 
    constr += [X[0,0] == init[0], X[1,0] == init[1], X[2,0] == init[2]]
    for t in range(T):
        # (6.1)
        if t > 0:
            for p in range(nb_path):
                constr += [0. <= U[p,t], U[p,t] <= W_bar[p]]
        elif t == 0:
            for p in range(nb_path):
                constr += [0. <= U[p,t], U[p,t] <= W_current[p]]
        constr += [U[0,t] + X[0,t] + U[1,t] + X[1,t] - L[0,t] == 0., L[0,t] <= C[0]]
        constr += [U[2,t] + X[2,t] - L[1,t] == 0., L[1,t] <= C[1]]
        constr += [U[1,t] + X[1,t] - L[2,t] == 0., L[2,t] <= C[2]]
        constr += [U[0,t] + X[0,t] - L[3,t] == 0., L[3,t] <= C[3]]
        constr += [U[1,t] + X[1,t] + U[2,t] + X[2,t] - L[4,t] == 0., L[4,t] <= C[4]]
        # (6.5)
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[3] - L[3,t]) <= D[0] ]
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[2] - L[2,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[1]]
        constr += [cp.inv_pos(C[1] - L[1,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[2]]
        # (6.2)
        if t < (T-1):
            for p in range(nb_path):
                constr += [X[p,t+1] - (X[p,t] + U[p,t])*Q[p] == 0.]
        # (6.6)
        utility += (U[0,t]+X[0,t])**alpha[0]/alpha[0] + (U[1,t]+X[1,t])**alpha[1]/alpha[1] \
                            + (U[2,t]+X[2,t])**alpha[2]/alpha[2]
    # solve
    problem = cp.Problem(cp.Maximize(utility), constr)
    res = problem.solve(solver = cp.SCS)
    #print("Optimal value: ", res)
    #print("U: ", U.value)
    u = U.value
    u_first = [u[0,0],u[1,0],u[2,0]]
    return res,u_first

def update_policy(current_horizon, current_state, current_utility, W_bar, Q, C, D, alpha, sigma):
    #print(current_state)
    if current_horizon == 0:
        return current_utility
    nb_path = 3
    W_current = np.zeros(nb_path)
    next_state = np.zeros(nb_path)
    for p in range(nb_path):
        W_current[p] = W_bar[p] + simulate_TN(0., sigma, -W_bar[p], W_bar[p])
    U_current = np.array(solve_cp_post(current_horizon, current_state, W_current, W_bar, Q, C, D, alpha)[1])
    state_det = current_state + U_current
    #print(state_det)
    current_utility += state_det[0]**alpha[0]/alpha[0] + state_det[1]**alpha[1]/alpha[1] \
                          + state_det[2]**alpha[2]/alpha[2]
    for p in range(nb_path):
        next_state[p] = state_det[p]*Q[p] + \
        simulate_TN(0,sigma,-state_det[p]*min(Q[p], 1-Q[p]),state_det[p]*min(Q[p], 1-Q[p]))
    current_horizon -= 1
    current_state = next_state
    return update_policy(current_horizon, current_state, current_utility, W_bar, Q, C, D, alpha, sigma)

def solve_cp_prio(T, init, W_bar, Q, C, D, alpha):
    nb_link = 5
    nb_path = 3
    horizon = T
    X = cp.Variable((nb_path, horizon))
    U = cp.Variable((nb_path, horizon))
    L = cp.Variable((nb_link, horizon))
    constr = []
    utility = 0.
    # Initial time-step 
    constr += [X[0,0] == init[0], X[1,0] == init[1], X[2,0] == init[2]]
    for t in range(T):
        # (6.1)
        for p in range(nb_path):
            constr += [0. <= U[p,t], U[p,t] <= W_bar[p]]
        constr += [U[0,t] + X[0,t] + U[1,t] + X[1,t] - L[0,t] == 0., L[0,t] <= C[0]]
        constr += [U[2,t] + X[2,t] - L[1,t] == 0., L[1,t] <= C[1]]
        constr += [U[1,t] + X[1,t] - L[2,t] == 0., L[2,t] <= C[2]]
        constr += [U[0,t] + X[0,t] - L[3,t] == 0., L[3,t] <= C[3]]
        constr += [U[1,t] + X[1,t] + U[2,t] + X[2,t] - L[4,t] == 0., L[4,t] <= C[4]]
        # (6.5)
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[3] - L[3,t]) <= D[0] ]
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[2] - L[2,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[1]]
        constr += [cp.inv_pos(C[1] - L[1,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[2]]
        # (6.2)
        if t < (T-1):
            for p in range(nb_path):
                constr += [X[p,t+1] - (X[p,t] + U[p,t])*Q[p] == 0.]
        # (6.6)
        utility += (U[0,t]+X[0,t])**alpha[0]/alpha[0] + (U[1,t]+X[1,t])**alpha[1]/alpha[1] \
                       + (U[2,t]+X[2,t])**alpha[2]/alpha[2]
    # solve
    problem = cp.Problem(cp.Maximize(utility), constr)
    res = problem.solve(solver = cp.SCS)
    u = U.value
    #print("Optimal value: ", res)
    return u

def U_projection(W_current, current_state, U_det, Q, C, D):
    nb_link = 5
    nb_path = 3
    U = cp.Variable((nb_path))
    L = cp.Variable((nb_link))
    X = current_state
    constr = []
    # (6.1)
    for p in range(nb_path):
        constr += [0. <= U[p], U[p] <= W_current[p]]
    constr += [U[0] + X[0] + U[1] + X[1] - L[0] == 0., L[0] <= C[0]]
    constr += [U[2] + X[2] - L[1] == 0., L[1] <= C[1]]
    constr += [U[1] + X[1] - L[2] == 0., L[2] <= C[2]]
    constr += [U[0] + X[0] - L[3] == 0., L[3] <= C[3]]
    constr += [U[1] + X[1] + U[2] + X[2] - L[4] == 0., L[4] <= C[4]]    
    # (6.5)
    constr += [cp.inv_pos(C[0] - L[0]) + cp.inv_pos(C[3] - L[3]) <= D[0] ]
    constr += [cp.inv_pos(C[0] - L[0]) + cp.inv_pos(C[2] - L[2]) + cp.inv_pos(C[4] - L[4]) <= D[1]]
    constr += [cp.inv_pos(C[1] - L[1]) + cp.inv_pos(C[4] - L[4]) <= D[2]]
    # objective
    utility = (U_det[0]-U[0])**2 + (U_det[1]-U[1])**2 + (U_det[2]-U[2])**2 
    # solve
    problem = cp.Problem(cp.Minimize(utility), constr)
    res = problem.solve(solver = cp.SCS)
    u = U.value
    return u

def projection_policy(current_horizon, current_state, current_utility, W_bar, Q, C, D, alpha, sigma):
    u_star = solve_cp_prio(current_horizon, current_state, W_bar, Q, C, D, alpha)
    nb_path = 3
    W_current = np.zeros(nb_path)
    next_state = np.zeros(nb_path)
    for t in range(current_horizon):
        for p in range(nb_path):
            W_current[p] = W_bar[p] + simulate_TN(0., sigma, -W_bar[p], W_bar[p])
        U_current = U_projection(W_current, current_state, u_star[:,t], Q, C, D) 
        state_det = current_state + U_current
        #print(state_det)
        current_utility += state_det[0]**alpha[0]/alpha[0] + state_det[1]**alpha[1]/alpha[1] \
                                    + state_det[2]**alpha[2]/alpha[2]
        for p in range(nb_path):
            next_state[p] = state_det[p]*Q[p] + \
            simulate_TN(0,sigma,-state_det[p]*min(Q[p], 1-Q[p]),state_det[p]*min(Q[p], 1-Q[p]))
        current_state = next_state    
    return current_utility

def solve_cp_prio_more(T, init, W_bar, Q, C, D, alpha):
    nb_link = 5
    nb_path = 3
    horizon = T
    X = cp.Variable((nb_path, horizon))
    U = cp.Variable((nb_path, horizon))
    L = cp.Variable((nb_link, horizon))
    constr = []
    utility = 0.
    # Initial time-step 
    constr += [X[0,0] == init[0], X[1,0] == init[1], X[2,0] == init[2]]
    for t in range(T):
        # (6.1)
        for p in range(nb_path):
            constr += [0. <= U[p,t], U[p,t] <= W_bar[p]]
        constr += [U[0,t] + X[0,t] + U[1,t] + X[1,t] - L[0,t] == 0., L[0,t] <= C[0]]
        constr += [U[2,t] + X[2,t] - L[1,t] == 0., L[1,t] <= C[1]]
        constr += [U[1,t] + X[1,t] - L[2,t] == 0., L[2,t] <= C[2]]
        constr += [U[0,t] + X[0,t] - L[3,t] == 0., L[3,t] <= C[3]]
        constr += [U[1,t] + X[1,t] + U[2,t] + X[2,t] - L[4,t] == 0., L[4,t] <= C[4]]
        # (6.5)
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[3] - L[3,t]) <= D[0] ]
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[2] - L[2,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[1]]
        constr += [cp.inv_pos(C[1] - L[1,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[2]]
        # (6.2)
        if t < (T-1):
            for p in range(nb_path):
                constr += [X[p,t+1] - (X[p,t] + U[p,t])*Q[p] == 0.]
        # (6.6)
        utility += (U[0,t]+X[0,t])**alpha[0]/alpha[0] + (U[1,t]+X[1,t])**alpha[1]/alpha[1] \
                       + (U[2,t]+X[2,t])**alpha[2]/alpha[2]
    # solve
    problem = cp.Problem(cp.Maximize(utility), constr)
    res = problem.solve(solver = cp.SCS)
    u = U.value
    x = X.value
    #print("Optimal value: ", res)
    return u,x

def check_cp_degeneracy(T, init, W_bar, Q, C, D, alpha):
    nb_link = 5
    nb_path = 3
    horizon = T
    X = cp.Variable((nb_path, horizon))
    U = cp.Variable((nb_path, horizon))
    L = cp.Variable((nb_link, horizon))
    constr = []
    utility = 0.
    # Initial time-step 
    constr += [X[0,0] == init[0], X[1,0] == init[1], X[2,0] == init[2]]
    for t in range(T):
        # (6.1)
        for p in range(nb_path):
            constr += [-U[p,t] <= 0., U[p,t] - W_bar[p] <= 0.]
        constr += [U[0,t] + X[0,t] + U[1,t] + X[1,t] - L[0,t] == 0., L[0,t] - C [0] <= 0.]
        constr += [U[2,t] + X[2,t] - L[1,t] == 0., L[1,t] - C[1] <= 0.]
        constr += [U[1,t] + X[1,t] - L[2,t] == 0., L[2,t] - C[2] <= 0.]
        constr += [U[0,t] + X[0,t] - L[3,t] == 0., L[3,t] - C[3] <= 0.]
        constr += [U[1,t] + X[1,t] + U[2,t] + X[2,t] - L[4,t] == 0., L[4,t] - C[4] <= 0.]
        # (6.5)
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[3] - L[3,t]) - D[0] <= 0. ]
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[2] - L[2,t]) + cp.inv_pos(C[4] - L[4,t]) - D[1] <= 0.]
        constr += [cp.inv_pos(C[1] - L[1,t]) + cp.inv_pos(C[4] - L[4,t]) - D[2] <= 0.]
        # (6.2)
        if t < (T-1):
            for p in range(nb_path):
                constr += [X[p,t+1] - (X[p,t] + U[p,t])*Q[p] == 0.]
        # (6.6)
        utility += (U[0,t]+X[0,t])**alpha[0]/alpha[0] + (U[1,t]+X[1,t])**alpha[1]/alpha[1] \
                       + (U[2,t]+X[2,t])**alpha[2]/alpha[2]
    # Separate the equality and inequality constraints
    equality_constraints = []
    inequality_constraints = []
    for constraint in constr:
        if is_equality(constraint):
            equality_constraints.append(constraint)
        else:
            inequality_constraints.append(constraint)
    # solve
    problem = cp.Problem(cp.Maximize(utility), constr)
    res = problem.solve(solver = cp.SCS)
    #print("optimal value is "+str(res))
    tol = 1e-4
    #active_constr = [constraint for constraint in inequality_constraints if abs(constraint.dual_value) >= tol]
    # Check the strict complementary slackness condition
    strict_complementary = True
    for i,constraint in enumerate(inequality_constraints):
        lhs_value = constraint.args[0].value  # Left-hand side of the constraint
        rhs_value = constraint.args[1].value  # Right-hand side of the constraint
        constraint_difference = lhs_value - rhs_value
        is_active = abs(constraint_difference) <= tol
        if is_active:
            # Constraint is active, dual value should be non-zero
            if abs(constraint.dual_value) <= tol:
                strict_complementary = False
                print("problem occurs at constraint " + str(i+1) + ": " + str(abs(constraint.dual_value)))
                break
    if strict_complementary:
        print("Strict complementary slackness condition is satisfied.")
    else:
        print("Strict complementary slackness condition is not satisfied.")
        
def check_proj_degeneracy(T, init, W_bar, Q, C, D, alpha):
    u_star,x_star = solve_cp_prio_more(T, init, W_bar, Q, C, D, alpha)
    nb_link = 5
    nb_path = 3
    all_info = []
    for t in range(T):
        U = cp.Variable((nb_path))
        L = cp.Variable((nb_link))
        X = x_star[:,t]
        U_det = u_star[:,t]
        constr = []
        # (6.1)
        for p in range(nb_path):
            constr += [-U[p] <= 0., U[p] - W_bar[p] <= 0.]
        constr += [U[0] + X[0] + U[1] + X[1] - L[0] == 0., L[0] - C [0] <= 0.]
        constr += [U[2] + X[2] - L[1] == 0., L[1] - C[1] <= 0.]
        constr += [U[1] + X[1] - L[2] == 0., L[2] - C[2] <= 0.]
        constr += [U[0] + X[0] - L[3] == 0., L[3] - C[3] <= 0.]
        constr += [U[1] + X[1] + U[2] + X[2] - L[4] == 0., L[4] - C[4] <= 0.]
        # (6.5)
        constr += [cp.inv_pos(C[0] - L[0]) + cp.inv_pos(C[3] - L[3]) - D[0] <= 0. ]
        constr += [cp.inv_pos(C[0] - L[0]) + cp.inv_pos(C[2] - L[2]) + cp.inv_pos(C[4] - L[4]) - D[1] <= 0.]
        constr += [cp.inv_pos(C[1] - L[1]) + cp.inv_pos(C[4] - L[4]) - D[2] <= 0.]
        # objective
        utility = (U_det[0]-U[0])**2 + (U_det[1]-U[1])**2 + (U_det[2]-U[2])**2
        # Separate the equality and inequality constraints
        equality_constraints = []
        inequality_constraints = []
        for constraint in constr:
            if is_equality(constraint):
                equality_constraints.append(constraint)
            else:
                inequality_constraints.append(constraint)
        # solve
        problem = cp.Problem(cp.Minimize(utility), constr)
        res = problem.solve(solver = cp.SCS)
        tol = 1e-4
        # Check the strict complementary slackness condition
        strict_complementary = True
        for i,constraint in enumerate(inequality_constraints):
            lhs_value = constraint.args[0].value  # Left-hand side of the constraint
            rhs_value = constraint.args[1].value  # Right-hand side of the constraint
            constraint_difference = lhs_value - rhs_value
            is_active = abs(constraint_difference) <= tol
            if is_active:
                # Constraint is active, dual value should be non-zero
                if abs(constraint.dual_value) <= tol:
                    strict_complementary = False
                    print("problem occurs at constraint " + str(i+1) + ": " + str(abs(constraint.dual_value)))
                    break
        all_info.append(strict_complementary)
    return all_info

def solve_cp_post_more(T, init, W_current, W_bar, Q, C, D, alpha):
    nb_link = 5
    nb_path = 3
    horizon = T
    X = cp.Variable((nb_path, horizon))
    U = cp.Variable((nb_path, horizon))
    L = cp.Variable((nb_link, horizon))
    constr = []
    utility = 0.
    # Initial time-step 
    constr += [X[0,0] == init[0], X[1,0] == init[1], X[2,0] == init[2]]
    for t in range(T):
        # (6.1)
        if t > 0:
            for p in range(nb_path):
                constr += [0. <= U[p,t], U[p,t] <= W_bar[p]]
        elif t == 0:
            for p in range(nb_path):
                constr += [0. <= U[p,t], U[p,t] <= W_current[p]]
        constr += [U[0,t] + X[0,t] + U[1,t] + X[1,t] - L[0,t] == 0., L[0,t] <= C[0]]
        constr += [U[2,t] + X[2,t] - L[1,t] == 0., L[1,t] <= C[1]]
        constr += [U[1,t] + X[1,t] - L[2,t] == 0., L[2,t] <= C[2]]
        constr += [U[0,t] + X[0,t] - L[3,t] == 0., L[3,t] <= C[3]]
        constr += [U[1,t] + X[1,t] + U[2,t] + X[2,t] - L[4,t] == 0., L[4,t] <= C[4]]
        # (6.5)
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[3] - L[3,t]) <= D[0] ]
        constr += [cp.inv_pos(C[0] - L[0,t]) + cp.inv_pos(C[2] - L[2,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[1]]
        constr += [cp.inv_pos(C[1] - L[1,t]) + cp.inv_pos(C[4] - L[4,t]) <= D[2]]
        # (6.2)
        if t < (T-1):
            for p in range(nb_path):
                constr += [X[p,t+1] - (X[p,t] + U[p,t])*Q[p] == 0.]
        # (6.6)
        utility += (U[0,t]+X[0,t])**alpha[0]/alpha[0] + (U[1,t]+X[1,t])**alpha[1]/alpha[1] \
                            + (U[2,t]+X[2,t])**alpha[2]/alpha[2]
    # solve
    problem = cp.Problem(cp.Maximize(utility), constr)
    problem.solve(solver = cp.SCS)
    u = U.value
    x = X.value
    return u,x

def hybrid_policy(current_horizon, current_state, current_utility, W_current, W_bar, \
                        Q, C, D, alpha, sigma, count = 0, thres = 1.):
    if current_horizon == 0:
        return current_utility, count-1
    nb_path = 3
    u_star,x_star = solve_cp_post_more(current_horizon, current_state, W_current, W_bar, Q, C, D, alpha)
    count += 1
    horizon_left = current_horizon
    W_current = np.zeros(nb_path)
    for t in range(horizon_left):
        if t == 0 and count > 1:
            next_state = np.zeros(nb_path)
            U_current = u_star[:,t]
            state_det = current_state + U_current
            current_utility += state_det[0]**alpha[0]/alpha[0] + state_det[1]**alpha[1]/alpha[1] \
                                    + state_det[2]**alpha[2]/alpha[2]
            for p in range(nb_path):
                next_state[p] = state_det[p]*Q[p] + \
                simulate_TN(0,sigma,-state_det[p]*min(Q[p], 1-Q[p]),state_det[p]*min(Q[p], 1-Q[p]))
            current_state = next_state
            current_horizon -= 1
        else:    
            for p in range(nb_path):
                W_current[p] = W_bar[p] + simulate_TN(0., sigma, -W_bar[p], W_bar[p])
            U_current = U_projection(W_current, current_state, u_star[:,t], Q, C, D)
            diff = euclidean_distance(current_state,x_star[:,t])+euclidean_distance(W_current,W_bar) \
                           +euclidean_distance(U_current,u_star[:,t])
            if diff >= thres:
                break
            next_state = np.zeros(nb_path)
            state_det = current_state + U_current
            current_utility += state_det[0]**alpha[0]/alpha[0] + state_det[1]**alpha[1]/alpha[1] \
                                    + state_det[2]**alpha[2]/alpha[2]
            for p in range(nb_path):
                next_state[p] = state_det[p]*Q[p] + \
                simulate_TN(0,sigma,-state_det[p]*min(Q[p], 1-Q[p]),state_det[p]*min(Q[p], 1-Q[p]))
            current_state = next_state
            current_horizon -= 1
    return hybrid_policy(current_horizon, current_state, current_utility, \
                            W_current, W_bar, Q, C, D, alpha, sigma, count, thres)
